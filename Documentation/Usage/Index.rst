
.. include:: /Includes.rst.txt


.. _usage:

=====
Usage
=====

.. note::
   Only users with system maintainer rights can use this extension.

Go to the module :guilabel:`Admin tools > Grid migration`:

.. image:: /Images/migrate_modul.jpg
  :width: 200
  :alt: "Grid migration" Module

.. _grid-elements-in-use:

What grid elements do i have on my site?
========================================

Click on :guilabel:`click here` in the important notice box in the first step.
After that the extension analyzes your page and also gives you first
instructions.

.. image:: /Images/whatforgridelements.jpg
  :width: 200
  :alt: Analyse the website

.. _migrate_from_gridelements:

Migrate grid elements from gridelements layout key
===================================================

Click on :guilabel:`Migration form gridelements layout key` in the dropdown to
migrate all elements on the web page using layout key.

.. image:: /Images/migrateall.jpg
  :width: 200
  :alt: Migrate all from layout key


.. _clean-up:

Clean up the database
=====================

Search in the database for content elements that still use the :sql:`colpos`
`-1`:

.. code-block:: sql

   SELECT * FROM tt_content where colpos=-1;

In most cases all content elements that had been deleted in TYPO3 can also
be deleted from the database.

.. warning::

   This step cannot be undone! Make a backup first

.. code-block:: sql

   DELETE FROM tt_content where colpos=-1 and deleted=1;

Then make a database compare at
:guilabel:`Admin tools > Maintenance > Analyze Database`.


.. include:: /Includes.rst.txt

.. _introduction:

============
Introduction
============

.. _what-it-does:

What does it do?
================

This extension can be used to migrate a project from EXT:gridelements to
EXT:container.

For each gridelement used in your installation you can manually chose to which
container element it should be migrated. By default the column numbers are kept.
However, it is also possible to adjust the column number of the related content
elements.

This extension should only be used on development systems. It is not
recommended to use it on productive systems. It can only be used by
administrators with system maintainer rights.

.. figure:: /Images/migrate_modul.jpg
   :class: with-shadow
   :alt: The migration module

   The migration module

See the :ref:`Quick start <quick-start>` to get started.
